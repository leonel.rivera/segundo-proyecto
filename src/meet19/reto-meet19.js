const express = require('express');
const app = express();
const host = 'http://localhost:';
const port = 3000;
const fetch = require('node-fetch');

// sirve para recibir req
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let ciudades = [
    {
        nombre: "Sao Paulo",
        temperatura: 0
    },
    {
        nombre: "Lima",
        temperatura: 0
    },
    {
        nombre: "Santiago",
        temperatura: 0
    },
    {
        nombre: "Vancouver",
        temperatura: 0
    },
    {
        nombre: "New York",
        temperatura: 0
    },
    {
        nombre: "Praia",
        temperatura: 0
    },
    {
        nombre: "Durban",
        temperatura: 0
    },
    {
        nombre: "Sydney",
        temperatura: 0
    },
    {
        nombre: "Weno",
        temperatura: 0
    },
    {
        nombre: "Seoul",
        temperatura: 0
    }
];
/////////////////////function
const randomCity = () => {
    let random = Math.floor(Math.random() * 9);
    return random;
}

const listCiudades = () => {
    return tempCiudad();
}

const tempCiudad = async (temp) => {
    let arrayTemp = [];
    console.log('temp '+temp);
    for (let i = 0; i < 3; i++) {
        let ciudadTemp = ciudades[randomCity()].nombre;
        await tempPromesa(ciudadTemp);
        let temperatura = temp;
        console.log(temperatura+'  '+ciudadTemp);
        arrayTemp.push(ciudadTemp);
        arrayTemp.push(temperatura);
    }
    return arrayTemp;
}

//////////////////////////funcion/////////////////////

/////////////////////////endpoint
app.get('/', (req, res) => {
    const msj = listCiudades();
    res.json(msj);
});
///////////////////endpoint///////////////////////////////

////////////////////////////promesa
const tempPromesa = async (ciudad) => {
    let API = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad}&appid=39e5b168d05dc95006306ee67f5ae008`;
    console.log(API);
    const url = API;
    await fetch(url)
        .then(response => response.json())
        .then(json => {
            console.log((json.main.temp - 273.15).toFixed(2));
            tempCiudad((json.main.temp - 273.15).toFixed(2));
            //tempCiudad(20);
        })
        .catch(err => console.log(`falló: ${err.message}`))
};
//console.log(ciudades[randomCity()].nombre);
//tempPromesa(ciudades[randomCity()].nombre);
/////////////////////////////promesa*//////////////////////

app.listen(port, () => {
    console.log(`Listening ${host}${port}`);
});