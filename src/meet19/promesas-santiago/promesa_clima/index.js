const express = require('express');
const funciones = require('./functions');
const ciudades = require('./ciudades');
const app = express();
const port = 3021;
const host = 'http://localhost';
const url = `${host}:${port}`;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', function (req, res) {
    res.json(funciones.getCities(ciudades));
});

app.get('/random', function (req, res) {
  res.json(funciones.get3Cities(ciudades));
});

app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
});