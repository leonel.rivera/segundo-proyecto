
const fetch = require('node-fetch');
const getRandomNumber = (length) => Math.floor(Math.random() * length);
const URL = `https://api.openweathermap.org/data/2.5/weather?q=`;
const apiKey = `047af17d4013f8481bffc612494275c7`;

const getWeather = (city) => {
  const api = `${URL}${city}&appid=${apiKey}`;
  console.log(api);
  fetch(api)
    .then(res => res.json())
    .then(data => {
      console.log(data.main);
      console.log(data.name);
    }
  )
  .catch(err => console.log(err));
};

const getCities = (cities) => cities;

const get3Cities  = (cities) => {
  let randomCities = [];
  for(let i = 0; i < 3; i++) {
    let randomCityindex = getRandomNumber(cities.length);
    let randomCity = cities[randomCityindex];
    //console.log(randomCity);
    getWeather(randomCity)
    randomCities.push(randomCity);
  }
  return randomCities;
};

module.exports = {
  getCities,
  get3Cities
}