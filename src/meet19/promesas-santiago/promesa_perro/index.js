const main = document.getElementById('main');
const API = 'https://dog.ceo/api/breeds/image/random';

const createImg = (json) => {
  const dogImg = document.createElement('img');
  dogImg.src = json.message;
  main.appendChild(dogImg);
}

const getDogImg = (url) => {
  fetch(url)
  .then(response => response.json())
  .then(json => createImg(json))
  .catch(err => console.log(`falló: ${err.message}`))
}

getDogImg(API);