//esto es un callback, no una promesa
const saludar = (callback) => {
  console.log(`estoy en saludar`);
  callback();
}

const bienvenida = () => {
  console.log(`Te damos la Bienvenida`);
}

const despedida = () => {
  console.log(`hasta luego`);
}


saludar(bienvenida);
saludar(despedida);