const fetch = require('node-fetch');

function github1(username){
  const endpoint = `https://api.github.com/users/${username}`;
  fetch(endpoint)
  .then(response => response.json())
  .then(user => console.log(user))
}

//github1('santiaguf');

async function github2(username){
  const endpoint = `https://api.github.com/users/${username}`;
  fetch(endpoint)
  .then(response => response.json())
  .then(user => console.log(user))
}

//github2('falangess');

async function github3(username){
  const endpoint = `https://api.github.com/users/${username}`;
  const respuesta = await fetch(endpoint)
    if(respuesta.status !== 200){
    throw Error(`usuario ${username} no fue encontrado`);
    }
  const usuario = await respuesta.json();
  return usuario;
}

(async function () {
  try{
      let respuesta = await github3('falanges');
      console.log(respuesta);
  }
  catch(error) {
      console.log(error);
  }
})();