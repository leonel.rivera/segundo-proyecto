const apikey = `7fb789f2`;

let searchInput = document.getElementById('seach');
let searchButton = document.getElementById('seachBtn');
let result = document.getElementById('result');

async function getMovie(url) {

  const response = await fetch(url);
  const data = await response.json();
  return data;
};

const printData = (json) => {
  result.innerHTML = `
    <div class="card">
      <div class="card-image">
        <img src="${json.Poster}">
      </div>
      <div class="card-content">
        <h3>${json.Title}</h3>
        <p>${json.Plot}</p>
      </div>
    </div>
  `;
}

searchButton.addEventListener('click', () => {
  let theMovie = searchInput.value;
  const arrayMovie = theMovie.split('');
  let url = (arrayMovie[0] === 't' && arrayMovie[1] === 't' && arrayMovie[2] < 10) ? `https://omdbapi.com/?apikey=${apikey}&i=${theMovie}` : `https://omdbapi.com/?apikey=${apikey}&t=${theMovie}`;

  getMovie(url)
  .then(response => printData(response))
  .catch(error => console.log(error));
});