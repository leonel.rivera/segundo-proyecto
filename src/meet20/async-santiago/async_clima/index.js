const ApiKey = '1c67ae33adc312d33476ae4d68a09beb';

let searchInput = document.getElementById('seach');
let searchButton = document.getElementById('seachBtn');
let result = document.getElementById('result');

function search(thecity) {
  async function searchWeather(city){
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${ApiKey}`;
    const response = await fetch(url);
    const info = await response.json();
    return info;
  }

  let info = searchWeather(thecity);
  info.then(response => {
    const temperatureInKelvin = response.main.temp;
    const temperatureInCelsius = parseFloat(temperatureInKelvin - 273.15).toFixed(2);
    const responseText = `El clima de ${thecity} es de ${temperatureInCelsius} °C`;
    result.innerHTML = responseText;
  })
}

searchButton.addEventListener('click', () => {
  let theCity = searchInput.value;
  search(theCity);
  console.log(theCity);
});

