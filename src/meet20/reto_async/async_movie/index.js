const APIkey = '7fb789f2';
//const fetch = require('node-fetch');

let searchInput = document.getElementById('search');
let searchButton = document.getElementById('searchBtn');
let result = document.getElementById('result');


async function getMovie(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
};

const printData = (json) => {
  result.innerHTML = `
  <div class="card">
    <div class="card-image">
      <img src="${json.Poster}">
    </div>
    <div class="card-content">
      <h3>${json.Title}</h3>
      <p>${json.Plot}</p>
    </div>
  </div>
  `;
}

searchButton.addEventListener('click', () => {
  let theMovie = searchInput.value;
  console.log('APIkey '+APIkey);
  let url = `https://omdbapi.com/?apikey=${APIkey}&t=${theMovie}`;
  getMovie(url)
  .then(response => printData(response))
  .catch(error => console.error(error));
});