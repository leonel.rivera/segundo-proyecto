-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2021 a las 05:46:29
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `meet22-reto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bandas_musicales`
--

CREATE TABLE `bandas_musicales` (
  `id_banda` int(2) NOT NULL,
  `nombre_banda` varchar(40) NOT NULL,
  `integrantes` int(2) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_separacion` date DEFAULT NULL,
  `pais` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bandas_musicales`
--

INSERT INTO `bandas_musicales` (`id_banda`, `nombre_banda`, `integrantes`, `fecha_inicio`, `fecha_separacion`, `pais`) VALUES
(1, 'Patricio Rey y sus Redonditos de Ricota', 5, '1976-01-01', '2001-01-01', 'AR'),
(2, 'AC/DC', 5, '1973-01-01', NULL, 'AUS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bandas_musicales`
--
ALTER TABLE `bandas_musicales`
  ADD PRIMARY KEY (`id_banda`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bandas_musicales`
--
ALTER TABLE `bandas_musicales`
  MODIFY `id_banda` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
