--obtien todas las bandas
SELECT * FROM `bandas_musicales`

--obtiene todas las bandas de tu pais
SELECT * FROM `bandas_musicales` WHERE pais = 'AR'

--una banda solista
SELECT * FROM `bandas_musicales` WHERE integrantes = 1 LIMIT 1

--todas las canciones publicadas despues del 2015-2015
SELECT * FROM `canciones` WHERE year(fecha_publicacion) > 2015

--todas las canciones que duren mas de 3min
SELECT * FROM `canciones` WHERE duracion > 3

--todos los albumes
SELECT * FROM `albumes`