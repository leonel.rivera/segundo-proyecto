const mongoose = require('../conexion')
const express = require('express');
const app = express();
const port = 3025;
const host = 'http://localhost';
const url = `${host}:${port}`;
const { Schema } = mongoose;



app.use(express.urlencoded({ extended: true }));
app.use(express.json());



// Definimos el esquema de la colección
const schemaContact = new Schema({
    telefono: Number,
    instagram: String,
    Direccion: String
});

const schemaUser = new Schema({
    nombre: String,
    apellido: String,
    email: String,
    contacto: [schemaContact]
});

const userModel = mongoose.model('usuarios', schemaUser);


const validateIfEmailExists = async (emailInUrl) => await userModel.exists({ email: emailInUrl });

const emailExists = (req, res, next) => {
    validateIfEmailExists(req.params.email)
        .then((result) => result ? next() : res.status(404).send('email not found'))
        .catch(() => res.status(404).json('email not found'));
}

const emailInUse = (req, res, next) => {
    validateIfEmailExists(req.body.email)
        .then((result) => result ? res.status(404).send('email in use') : next())
        .catch(() => res.status(404).json('error'));
}


app.post('/', emailInUse, (req, res) => {

    const createUser = async () => {

        let userToBeCreated = {
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email
        }

        const newUser = new userModel(userToBeCreated);
        const result = await newUser.save();
        return result;
    }

    const response = createUser();

    response
        .then(result => res.json(result))
        .catch(err => res.json(err));

});

app.post('/:email', emailExists, (req, res) => {


    const searchUser = async () => {
        const result = await userModel.findOne({ email: req.params.email });
        return result;
    }

    const response = searchUser();

    response
        .then(user => {
            user.contacto.push({
                telefono: req.body.telefono,
                instagram: req.body.instagram,
                Direccion: req.body.Direccion
            });
            user.save();
            res.json(user);
        })
        .catch(err => res.json(err));

});


app.listen(port, () => console.log(`Servidor corriendo en ${url}`));
