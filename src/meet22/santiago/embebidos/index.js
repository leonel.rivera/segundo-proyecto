const mongoose = require('../conexion');
const express = require('express');
const port = 3024;
const host = 'http://localhost';
const url = `${host}:${port}`;
const app = express();
const { Schema } = mongoose;

const createPost = async (post) => {
  const response = await post.save();
  return response;
}

const Comentario = new Schema({
  titulo: String,
  cuerpo: String,
  fecha: Date
});

const Posteo = new Schema({
  titulo: String,
  cuerpo: String,
  fecha: Date,
  comentarios: [Comentario]
});

const posteoModel = mongoose.model('posteos', Posteo);

const helloWorld = {
  titulo: 'Hola mundo',
  cuerpo: 'Este es el cuerpo del posteo',
  fecha: new Date(),
};

const newPost = new posteoModel(helloWorld);

const demoComment = {
  titulo: 'genial',
  cuerpo: 'Este es un comentario',
  fecha: new Date()
};

newPost.comentarios.push(demoComment);


createPost(newPost)
  .then(response =>  console.log(response))
  .catch(error =>  console.log(error));


app.listen(port, () => {
    console.log(`Servidor corriendo en ${url}`);
});
