const mongoose = require('../conexion');
const express = require('express');
const port = 3023;
const host = 'http://localhost';
const url = `${host}:${port}`;
const app = express();

async function insertarPlato() {
  const modeloPlatos = {
    plato: String,
    precio: Number,
    tipo_de_plato: String,
  }

const plato = mongoose.model('platos', modeloPlatos);

const milanesa = {
  plato: 'Milanesa',
  precio: 2,
  tipo_de_plato: 'Plato fuerte',
};

const nuevoPlato = new plato(milanesa);

const session = await mongoose.startSession();
session.startTransaction();

await nuevoPlato.save();

await session.commitTransaction();

// si quisieramos abortar la transaccion del
//await  session.abortTransaction();

session.endSession();

}

insertarPlato();

app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
})
