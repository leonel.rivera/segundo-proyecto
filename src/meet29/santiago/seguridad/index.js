
require('dotenv').config();

const jwt = require('jsonwebtoken');
const userInfo = {nombre: 'santi', edad: 31};
const signature = process.env.JWT_SECRET;

const token = jwt.sign(userInfo, signature);
console.log(token);


const decoded = jwt.verify(token, signature);
console.log(decoded);