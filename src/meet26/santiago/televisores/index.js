const express = require("express");
const app = express();
const PORT = process.env.PORT || 3030;
const connection = require("./config/db.config");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const televisorRoutes = require("./routes/tv.route");
const modeloRoutes = require("./routes/modelos.route");
const marcaRoutes = require("./routes/marcas.route");

app.use('/api/v1/televisores', televisorRoutes);
app.use('/api/v1/modelos', modeloRoutes);
app.use('/api/v1/marcas', marcaRoutes);


app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
  connection.authenticate().then(() => {
      console.log('BBDD conectada');
  }).catch((err) => {
      console.log('BBDD error', err);
  })
});
