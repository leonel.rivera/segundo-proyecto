require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const modeloModel = require('../models/modelos.model')(connection, Sequelize);
const marcaModel = require('../models/marcas.model')(connection, Sequelize);

const createModelo = async (req) => {
  const newModelo = await modeloModel.build({
    nombre_modelo: req.body.nombre_modelo,
    id_marca: req.body.id_marca
  });

  const result = await newModelo.save();
  return result;
}

const listModelo = async () => await modeloModel.findAll();

const updateModelo = async (req) => {
  const modeloId = parseInt(req.params.id);
  const result = await modeloModel.update({
    nombre_modelo: req.body.nombre_modelo,
    id_marca: req.body.id_marca
  },
    { where: { id_modelo: modeloId } }
  );
  return result;
}

const deleteModelo = async (req) => {
  const modeloId = req.params.id;
  const result = await modeloModel.destroy({
    where: { id_modelo: modeloId }
  });
  return result;
}

const listModeloById = async (req) => {
  const modeloId = req.params.id;
  const result = await modeloModel.findOne({ where: { id_modelo: modeloId } });
  return result;
}

const listMarcaByMarca = async (req) => {
  const marcaMarca = req.params.marca;
  const result = await modeloModel.findAll({ 
    include: 'marcas' });
  return result;
}

module.exports = {
  createModelo,
  listModelo,
  updateModelo,
  deleteModelo,
  listModeloById,
  listMarcaByMarca
}
