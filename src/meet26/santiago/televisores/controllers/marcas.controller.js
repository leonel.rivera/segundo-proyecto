require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const marcaModel = require('../models/marcas.model')(connection, Sequelize);


const createMarca = async (req) => {
  const newMarca = await marcaModel.build({
    nombre_marca: req.body.nombre_marca
  });

  const result = await newMarca.save();
  return result;
}

const listMarca = async () => await marcaModel.findAll();

const updateMarca = async (req) => {
  const marcaId = parseInt(req.params.id);
  const result = await marcaModel.update({
    nombre_marca: req.body.nombre_marca
  },
    { where: { id_marca: marcaId } }
  );
  return result;
}

const deleteMarca = async (req) => {
  const marcaId = req.params.id;
  const result = await marcaModel.destroy({
    where: { id_marca: marcaId }
  });
  return result;
}

const listMarcaById = async (req) => {
  const marcaId = req.params.id;
  const result = await marcaModel.findOne({ where: { id_marca: marcaId } });
  return result;
}

const listMarcaByMarca = async (req) => {
  const marcaMarca = req.params.marca;
  const result = await marcaModel.findAll({ 
    include: ['modelos'] });
  return result;
}


module.exports = {
  createMarca,
  listMarca,
  updateMarca,
  deleteMarca,
  listMarcaById,
  listMarcaByMarca
}
