require("dotenv").config();
const { Model } = require("sequelize");
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const televisorModel = require('../models/tv.model')(connection, Sequelize);
const marcaModel = require('../models/marcas.model')(connection, Sequelize);

const createTelevisor = async (req) => {
  const newTelevisor = await televisorModel.build({
    id_marca: req.body.id_marca,
    id_modelo: req.body.id_modelo,
    precio: req.body.precio,
    tamano_pantalla: req.body.tamano_pantalla,
    smart_tv: req.body.smart_tv
  });

  const result = await newTelevisor.save();
  return result;
}

const listTelevisor = async () => await televisorModel.findAll();

const updateTelevisor = async (req) => {
  const televisorId = parseInt(req.params.id);
  const result = await televisorModel.update({
    id_marca: req.body.id_marca,
    id_modelo: req.body.id_modelo,
    precio: req.body.precio,
    tamano_pantalla: req.body.tamano_pantalla,
    smart_tv: req.body.smart_tv
  },
    { where: { id_televisor: televisorId } }
  );
  return result;
}

const deleteTelevisor = async (req) => {
  const televisorId = req.params.id;
  const result = await televisorModel.destroy({
    where: { id_televisor: televisorId }
  });
  return result;
}

const listTelevisorById = async (req) => {
  const televisorId = req.params.id;
  const result = await televisorModel.findOne({ where: { id_televisor: televisorId } });
  return result;
}


module.exports = {
  createTelevisor,
  listTelevisor,
  updateTelevisor,
  deleteTelevisor,
  listTelevisorById
}
