const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const modeloModel = require('../models/modelos.model')(connection, Sequelize);

const Marcamodel = (connection, Sequelize) => {
  const Marca = connection.define('marcas', {
      id_marca: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nombre_marca: {
        type: Sequelize.STRING
      }
  },
  {
    timestamps: false
  });
  Marca.hasMany(modeloModel);
    modeloModel.belongsTo(Marca, {
      foreignKey: "id_marca"
    });
  return Marca
}

module.exports = Marcamodel;