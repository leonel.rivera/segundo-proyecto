const Modelomodel = (connection, Sequelize) => {
  const Modelo = connection.define('modelos', {
      id_modelo: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nombre_modelo: {
        type: Sequelize.STRING,
      },
      id_marca: {
        type: Sequelize.INTEGER
      }
  },
  {
    timestamps: false
  });
  return Modelo
}

module.exports = Modelomodel;