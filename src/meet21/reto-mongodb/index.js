require('dotenv').config();
const express = require('express');
const app = express();
const platos = require('./routes/platos');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/platos', platos);

app.listen(process.env.MONGODB_PORT, () => {
    console.log(`Listening http://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/`);
});