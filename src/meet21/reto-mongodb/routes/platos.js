const express = require('express');
const router = express.Router();
let funcPlato = require('../controllers/platos');
let middPlato = require('../middlewares/platos');


router.get('/listar', (req, res) => {
    funcPlato.listPlato()
        .then(platos => res.json(platos))
        .catch(err => res.json(err));
});

router.post('/crear', (req, res) => {
    funcPlato.addPlato(req.body)
        .then(plato => res.json(plato))
        .catch(err => res.json(err));
});

router.get('/buscar/:id',middPlato.platoExists, (req, res) => {
    funcPlato.getPlatoById(req.params.id)
    .then(platos => res.json(platos))
    .catch(err => res.json(err));
});

router.put('/modificar/:id',middPlato.platoExists, (req, res) => {
    funcPlato.modifyPlato(req.params.id, req.body)
    .then(plato => res.json(plato))
    .catch(err => res.json(err));
});

router.delete('/eliminar/:id',middPlato.platoExists, (req, res) => {
    funcPlato.deletePlato(req.params.id)
    .then(plato => res.json(plato))
    .catch(err => res.json(err));
});

module.exports = router;