const mongoose = require('../config/conexion');

const modeloPlatos = {
    plato: String,
    precio: Number,
    tipo_de_plato: String
}

const platos = mongoose.model('plato', modeloPlatos);

module.exports = platos;