let funcPlato = require('../controllers/platos');

const platoExists = (req, res, next) => {
    funcPlato.platoExists(req.params.id)
        .then((result) => result ? next() : res.status(404).send('Plato no encontrado'))
        .catch(() => res.status(404).json('Plato no encontrado'));
}

module.exports = {
    platoExists
};