let platos = require('../models/platos');

const listPlato = async () => await platos.find();

const addPlato = async (plato) => {
    const newPlatos = new platos(plato);
    const response = await newPlatos.save();
    return response;
}

const modifyPlato = async (platoId, plato) => await platos.findByIdAndUpdate(platoId, plato);

const deletePlato = async (platoId) => await platos.findByIdAndDelete(platoid);

const getPlatoById = async (platoId) => await platos.findById(platoId);

const platoExists = async (platoId) => await platos.exists({ _id: platoId });

module.exports = {
    listPlato,
    addPlato,
    modifyPlato,
    deletePlato,
    getPlatoById,
    platoExists
};