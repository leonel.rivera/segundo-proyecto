require('dotenv').config();
const jwt = require('jsonwebtoken');
const Plato = require('../platos/platos');


const listDishes = async () => await Plato.find();

const getDishById = async (dishId) => await Plato.findById(dishId);

const createDish = async (dish) => {
    const newPlatos = new Plato(dish);
    const response = await newPlatos.save();
    return response;
}
const updateDish = async (dishId, dish) => await Plato.findByIdAndUpdate(dishId, dish);

const deleteDish = async (dishId) => await Plato.findByIdAndDelete(dishId);

const dishExists = async (dishId) => await Plato.exists({ _id: dishId });

const login = (info) => {
    const username = info.username;
    const password = info.password;
    const data = { user : 'admin', password: 'clavesegura'};

    if (username === data.user && password === data.password) {
        const token = jwt.sign({ user: data.user }, process.env.JWT_SECRET, { expiresIn: 3600 });
        return { yourToken: token };
    } else {
        return 'inicio incorrecto';
    }

}


module.exports = {
    listDishes,
    getDishById,
    createDish,
    updateDish,
    deleteDish,
    dishExists,
    login
}