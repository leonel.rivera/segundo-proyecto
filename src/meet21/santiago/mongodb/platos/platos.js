const mongoose = require('../conexion');

const modeloPlatos = {
    plato: String,
    precio: Number,
    tipo_de_plato: String
}

const Plato = mongoose.model('platos', modeloPlatos);

module.exports = Plato;