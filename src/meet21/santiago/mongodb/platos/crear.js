const Platos = require('./platos');

const newPlatos = new Platos({
    plato: 'Pizza',
    precio: 10,
    tipo_de_plato: 'Entrada'
});

newPlatos.save(newPlatos)
    .then(() => console.log('Plato guardado'))
    .catch(err => console.log(err));