const Pelicula = require('./peliculas');

const newPelicula = new Pelicula({
  titulo: 'no respires',
  director: 'Rodo Sayague',
  genero: 'horror',
  lanzamiento: new Date()
});

newPelicula.save()
  .then(() => console.log('Pelicula guardada'))
  .catch(err => console.log(err));