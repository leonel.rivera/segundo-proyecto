const express = require('express');
const funciones = require('./funciones');
const app = express();
const port = 3022;
const host = 'http://localhost';
const url = `${host}:${port}`;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const dishExists = (req, res, next) => {
  funciones.dishExists(req.params.id)
  .then((result) => result ? next() : res.status(404).send('Dish not found'))
  .catch(() => res.status(404).json('Dish not found'));
}

app.get('/', function (req, res) {
  funciones.listDishes()
  .then(dishes => res.json(dishes))
  .catch(err => res.json(err));
});

app.get('/:id', dishExists, function (req, res) {
  funciones.getDishById(req.params.id)
  .then(dishes => res.json(dishes))
  .catch(err => res.json(err));
});

app.post('/', function (req, res) {
  funciones.createDish(req.body)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.put('/:id', dishExists, function (req, res) {
  funciones.updateDish(req.params.id, req.body)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.delete('/:id', dishExists, function (req, res) {
  funciones.deleteDish(req.params.id)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
});