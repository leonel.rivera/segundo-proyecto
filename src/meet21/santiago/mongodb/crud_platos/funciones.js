const Plato = require('../platos/platos');

const listDishes = async () => await Plato.find();

const getDishById = async (dishId) => await Plato.findById(dishId);

const createDish = async (dish) => {
    const newPlatos = new Plato(dish);
    const response = await newPlatos.save();
    return response;
}
const updateDish = async (dishId, dish) => await Plato.findByIdAndUpdate(dishId, dish);

const deleteDish = async (dishId) => await Plato.findByIdAndDelete(dishId);

const dishExists = async (dishId) => await Plato.exists({ _id: dishId });

module.exports = {
    listDishes,
    getDishById,
    createDish,
    updateDish,
    deleteDish,
    dishExists
}