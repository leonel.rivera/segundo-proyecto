const express = require("express");
const app = express();
const PORT = process.env.PORT || 3029;
require('./models/associations');
const Sequelize = require('sequelize');
const connection = require("./config/db.config");


app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const bandaRoutes = require("./routes/bandas.route");
const albumRoutes = require("./routes/albumes.route");
const cancionRoutes = require("./routes/canciones.route");

app.use('/api/bandas', bandaRoutes)
app.use('/api/albumes', albumRoutes)
app.use('/api/canciones', cancionRoutes)

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
    connection.authenticate().then(() => {
        console.log('BBDD conectada');
    }).catch((err) => {
        console.log('BBDD error', err);
    })
});