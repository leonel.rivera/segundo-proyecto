const Albummodel = (connection, Sequelize) => {
    const Album = connection.define('albumes', {
        id_album: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_album: {
            type: Sequelize.STRING
        },
        fecha_publicacion: {
            type: Sequelize.DATE
        },
        id_banda: {
            type: Sequelize.INTEGER,
        }
    },
        {
            timestamps: false
        });
    return Album
}

module.exports = Albummodel;