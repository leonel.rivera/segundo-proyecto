const Sequelize = require('sequelize');
const connection = require("../config/db.config.js");
const albumModel = require('./albumes.model')(connection, Sequelize);
const bandaModel = require('./bandas.model')(connection, Sequelize);
const cancionModel = require('./canciones.model')(connection, Sequelize);

bandaModel.hasMany(albumModel);
albumModel.belongsTo(bandaModel);