require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config.js");
const albumModel = require('../models/albumes.model')(connection, Sequelize);

const createAlbum = async (req) => {
  const albumDetails = await albumModel.build({
    nombre_album: req.body.nombre_album,
    fecha_publicacion: req.body.fecha_publicacion,
    id_banda: req.body.id_banda
  });

  const result = await albumDetails.save();
  return result;
}

const listAlbum = async () => await albumModel.findAll();

const updateAlbum = async (req) => {
  const id_album = parseInt(req.params.id);

  const result = await albumModel.update(
    {
      nombre_album: req.body.nombre_album,
      fecha_publicacion: req.body.fecha_publicacion,
      id_banda: req.body.id_banda
    },
    { where: { id_album: id_album } }
  );
  return result;
}

const deleteAlbum = async (req) => {
  const id_album = req.params.id;
  const result = await albumModel.destroy({
    where: { id_album: id_album }
  });
  return result;
}

const listAlbumByBandaId = async (req) => {
  const id_banda = req.params.id;
  const result = await albumModel.findAll({
    where: { id_banda: id_banda }
  });
  return result;
}

module.exports = {
  createAlbum,
  listAlbum,
  updateAlbum,
  deleteAlbum,
  listAlbumByBandaId
}
