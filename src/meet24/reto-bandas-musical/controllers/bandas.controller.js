require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config.js");
const bandaModel = require('../models/bandas.model')(connection, Sequelize);

const createBanda = async (req) => {
  const bandaDetails = await bandaModel.build({
    nombre_banda: req.body.nombre_banda,
    integrantes: req.body.integrantes,
    fecha_inicio: req.body.fecha_inicio,
    fecha_separacion: req.body.fecha_separacion,
    pais: req.body.pais
  });

  const result = await bandaDetails.save();
  return result;
}

const listBanda = async () => await bandaModel.findAll();

const updateBanda = async (req) => {
  const id_banda = parseInt(req.params.id);

  const result = await bandaModel.update(
    {
      nombre_banda: req.body.nombre_banda,
      integrantes: req.body.integrantes,
      fecha_inicio: req.body.fecha_inicio,
      fecha_separacion: req.body.fecha_separacion,
      pais: req.body.pais
    },
    { where: { id_banda: id_banda } }
  );
  return result;
}

const deleteBanda = async (req) => {
  const id_banda = req.params.id;
  const result = await bandaModel.destroy({
    where: { id_banda: id_banda }
  });
  return result;
}

const listBandaById = async (req) => {
  const id_banda = req.params.id;
  const result = await bandaModel.findAll({
    where: { id_banda: id_banda }
  });
  return result;
}

module.exports = {
  createBanda,
  listBanda,
  updateBanda,
  deleteBanda,
  listBandaById
}
