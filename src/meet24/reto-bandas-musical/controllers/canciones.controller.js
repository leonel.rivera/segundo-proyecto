require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config.js");
const cancionModel = require('../models/canciones.model')(connection, Sequelize);

const createCancion = async (req) => {
  const cancionDetails = await cancionModel.build({
    nombre_cancion: req.body.nombre_cancion,
    duracion: req.body.duracion,
    id_album: req.body.id_album,
    id_banda: req.body.id_banda,
    fecha_publicacion: req.body.fecha_publicacion
  });

  const result = await cancionDetails.save();
  return result;
}

const listCancion = async () => await cancionModel.findAll();

const updateCancion = async (req) => {
  const id_cancion = parseInt(req.params.id);

  const result = await cancionModel.update(
    {
      nombre_cancion: req.body.nombre_cancion,
      duracion: req.body.duracion,
      id_album: req.body.id_album,
      id_banda: req.body.id_banda,
      fecha_publicacion: req.body.fecha_publicacion
    },
    { where: { id_cancion: id_cancion } }
  );
  return result;
}

const deleteCancion = async (req) => {
  const id_cancion = req.params.id;
  const result = await cancionModel.destroy({
    where: { id_cancion: id_cancion }
  });
  return result;
}

const listCancionById = async (req) => {
  const id_album = req.params.id;
  const result = await cancionModel.findAll({
    where: { id_album: id_album }
  });
  return result;
}

module.exports = {
  createCancion,
  listCancion,
  updateCancion,
  deleteCancion,
  listCancionById
}
