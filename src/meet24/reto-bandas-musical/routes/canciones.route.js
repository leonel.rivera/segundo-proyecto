const express = require('express')
const router = express.Router()

const cancionController = require("../controllers/canciones.controller");

router.post("/", (req, res) => {
    cancionController.createCancion(req)
        .then(() => {
            res.status(200).send({
                status: 200,
                message: "Data Save Successfully",
            });
        })
        .catch(error => {
            res.status(400).send({
                message: "Unable to insert data",
                errors: error,
                status: 400
            });
        });
});

router.get("/", (req, res) => {
    cancionController.listCancion()
        .then((result) => {
            res.status(200).send({
                status: 200,
                message: "Data find Successfully",
                data: result
            });
        })
        .catch(error => {
            res.status(400).send({
                message: "Unable to find data",
                errors: error,
                status: 400
            });
        });
});

router.put("/:id", (req, res) => {
    cancionController.updateCancion(req)
        .then(() => {
            res.status(200).send({
                status: 200,
                message: "Data Update Successfully",
            });
        })
        .catch(error => {
            res.status(400).send({
                message: "Unable to Update data",
                errors: error,
                status: 400
            });
        });
});

router.delete("/:id", (req, res) => {
    cancionController.deleteCancion(req)
        .then(() => {
            res.status(200).send({
                status: 200,
                message: "Data Delete Successfully",
            });
        })
        .catch(error => {
            res.status(400).send({
                message: "Unable to Delete data",
                errors: error,
                status: 400
            });
        });
});

router.get("/filterbyid/:id", (req, res) => {
    cancionController.listCancionById(req)
        .then((result) => {
            res.status(200).send({
                status: 200,
                message: "Data find Successfully",
                data: result
            });
        })
        .catch(error => {
            res.status(400).send({
                message: "Unable to find data",
                errors: error,
                status: 400
            });
        });
});

module.exports = router;