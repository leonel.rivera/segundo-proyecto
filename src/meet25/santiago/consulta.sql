SELECT cp.`id_contacto_persona`, cp.`id_persona`, cp.`id_tipo_contacto`, cp.`valor`,
tc.`nombre_tipo_contacto`, p.`nombres_persona`, p.`apellidos_persona` 
FROM `contactos_persona` as cp
join `tipos_contacto` as tc
on cp.`id_tipo_contacto`=tc.`id_tipo_contacto`
join `personas` as p
on cp.`id_persona`=p.`id_persona`
where p.`id_persona`=1