--
-- Estructura de tabla para la tabla `contactos_persona`
--
​
CREATE TABLE `contactos_persona` (
  `id_contacto_persona` int(5) NOT NULL,
  `id_persona` int(4) NOT NULL,
  `id_tipo_contacto` int(2) NOT NULL,
  `valor` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
​
--
-- Volcado de datos para la tabla `contactos_persona`
--
​
INSERT INTO `contactos_persona` (`id_contacto_persona`, `id_persona`, `id_tipo_contacto`, `valor`) VALUES
(1, 2, 1, 'fb.com/cristian'),
(2, 1, 4, 'matias.parache@gmail.com'),
(3, 1, 4, 'mati@patagonia.org.ar'),
(4, 3, 5, '54911158696');
​
-- --------------------------------------------------------
​
--
-- Estructura de tabla para la tabla `personas`
--
​
CREATE TABLE `personas` (
  `id_persona` int(4) NOT NULL,
  `nombres_persona` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos_persona` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
​
--
-- Volcado de datos para la tabla `personas`
--
​
INSERT INTO `personas` (`id_persona`, `nombres_persona`, `apellidos_persona`) VALUES
(1, 'Matias', 'Parache'),
(2, 'Cristian', 'Villareal'),
(3, 'Javier', 'Oyarzo');
​
-- --------------------------------------------------------
​
--
-- Estructura de tabla para la tabla `tipos_contacto`
--
​
CREATE TABLE `tipos_contacto` (
  `id_tipo_contacto` int(2) NOT NULL,
  `nombre_tipo_contacto` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
​
--
-- Volcado de datos para la tabla `tipos_contacto`
--
​
INSERT INTO `tipos_contacto` (`id_tipo_contacto`, `nombre_tipo_contacto`) VALUES
(1, 'Facebook'),
(2, 'instagram'),
(3, 'twitter'),
(4, 'Correo'),
(5, 'telefono');
​
--
-- Índices para tablas volcadas
--
​
--
-- Indices de la tabla `contactos_persona`
--
ALTER TABLE `contactos_persona`
  ADD PRIMARY KEY (`id_contacto_persona`),
  ADD KEY `id_persona` (`id_persona`),
  ADD KEY `id_tipo_contacto` (`id_tipo_contacto`);
​
--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id_persona`);
​
--
-- Indices de la tabla `tipos_contacto`
--
ALTER TABLE `tipos_contacto`
  ADD PRIMARY KEY (`id_tipo_contacto`);
​
--
-- AUTO_INCREMENT de las tablas volcadas
--
​
--
-- AUTO_INCREMENT de la tabla `contactos_persona`
--
ALTER TABLE `contactos_persona`
  MODIFY `id_contacto_persona` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
​
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id_persona` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
​
--
-- AUTO_INCREMENT de la tabla `tipos_contacto`
--
ALTER TABLE `tipos_contacto`
  MODIFY `id_tipo_contacto` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
​
--
-- Restricciones para tablas volcadas
--
​
--
-- Filtros para la tabla `contactos_persona`
--
ALTER TABLE `contactos_persona`
  ADD CONSTRAINT `contactos_persona_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`),
  ADD CONSTRAINT `contactos_persona_ibfk_2` FOREIGN KEY (`id_tipo_contacto`) REFERENCES `tipos_contacto` (`id_tipo_contacto`);
COMMIT;